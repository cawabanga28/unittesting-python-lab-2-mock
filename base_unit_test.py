import unittest
import mock
import mailer


class TestFirst(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    @mock.patch('mailer.Mailer.send_mail')
    def test_000_send_message(self, mock_send_mail):
        body = "Example body for someone message"
        mailer.Mailer.send_mail("example@example.com", "example_message", body)
        mock_send_mail.assert_called_with("example@example.com", "example_message", body)




